# Package

version       = "1.5.0"
author        = "Adam Blažek"
description   = "CLI client for Bakaláři"
license       = "GPL-3.0"
srcDir        = "src"
bin           = @["bk"]



# Dependencies

requires "nim >= 1.2.4"

requires "cligen >= 1.2.0"
requires "colorize >= 0.2.0"
requires "elvis >= 0.2.0"
requires "zero_functional >= 1.2.0"
